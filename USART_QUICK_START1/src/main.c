/**
 * \file
 *
 * \brief SAM USART Quick Start
 *
 * Copyright (C) 2012-2015 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */
#include "asf.h"
// #include <ctype.h>
// #include <stdio.h>
// #include <string.h>
// #include <board.h>
#include "cryptoauthlib.h"
#include "test/atca_unit_tests.h"
#include "test/atca_basic_tests.h"
#include "test/atca_crypto_sw_tests.h"
#include "host/atca_host.h"
#include "stdio_serial.h"

void configure_usart(void);

struct usart_module usart_instance;

void configure_usart(void)
{
	struct usart_config config_usart;

	usart_get_config_defaults(&config_usart);
	config_usart.baudrate    = 115200;
	config_usart.mux_setting = EDBG_CDC_SERCOM_MUX_SETTING;
	config_usart.pinmux_pad0 = EDBG_CDC_SERCOM_PINMUX_PAD0;
	config_usart.pinmux_pad1 = EDBG_CDC_SERCOM_PINMUX_PAD1;
	config_usart.pinmux_pad2 = EDBG_CDC_SERCOM_PINMUX_PAD2;
	config_usart.pinmux_pad3 = EDBG_CDC_SERCOM_PINMUX_PAD3;

	while (usart_init(&usart_instance,EDBG_CDC_MODULE, &config_usart) != STATUS_OK) {
	}
	usart_enable(&usart_instance);
}

int main(void)
{
	system_init();

	configure_usart();

	delay_init();

	uint16_t temp;
	ATCA_STATUS status;
	
	gCfg = &cfg_ateccx08a_i2c_default;

	// Read DevRev
	uint8_t revision[4];
	char displaystr[15];
	int displaylen = sizeof(displaystr);

	uint8_t string[] = "Read DevRev!\r\n";
	usart_write_buffer_wait(&usart_instance, string, sizeof(string));

	atcab_init( gCfg );
	status =  atcab_info( revision );
	atcab_release();

	if ( status == ATCA_SUCCESS )
	{
		// check ECC508A device
		if ( revision[2] == 0x50 )
		{
			uint8_t string1[] = "ECC508A detected!\r\n";
			usart_write_buffer_wait(&usart_instance, string1, sizeof(string1));
			uint8_t string2[] = "Revision Number: ";
			usart_write_buffer_wait(&usart_instance, string2, sizeof(string2));
			atcab_bin2hex(revision, 4, displaystr, &displaylen );
			usart_write_buffer_wait(&usart_instance, displaystr, sizeof(displaystr));
			uint8_t string3[] = "\r\n";
			usart_write_buffer_wait(&usart_instance, string3, sizeof(string3));
		}
		else
		{
			uint8_t string1[] = "ECC508A not detected!\r\n";
			usart_write_buffer_wait(&usart_instance, string1, sizeof(string1));
		}
	}
	else
	{
		uint8_t string1[] = "ECC508A not detected!\r\n";
		usart_write_buffer_wait(&usart_instance, string1, sizeof(string1));
	}

	// Read Device s/n


// 	uint8_t string[] = "Hello World!\r\n";
// 	usart_write_buffer_wait(&usart_instance, string, sizeof(string));
	while (true) {
// 		if (usart_read_wait(&usart_instance, &temp) == STATUS_OK) {
// 			while (usart_write_wait(&usart_instance, temp) != STATUS_OK) {
// 			}
// 		}
	}

}
