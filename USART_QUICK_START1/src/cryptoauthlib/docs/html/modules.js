var modules =
[
    [ "Configuration (cfg_)", "a00171.html", "a00171" ],
    [ "ATCACommand (atca_)", "a00172.html", "a00172" ],
    [ "ATCADevice (atca_)", "a00173.html", "a00173" ],
    [ "ATCAIface (atca_)", "a00174.html", "a00174" ],
    [ "Certificate manipulation methods (atcacert_)", "a00175.html", "a00175" ],
    [ "Basic Crypto API methods (atcab_)", "a00176.html", "a00176" ],
    [ "Software crypto methods (atcac_)", "a00177.html", "a00177" ],
    [ "Hardware abstraction layer (hal_)", "a00178.html", "a00178" ],
    [ "Host side crypto methods (atcah_)", "a00179.html", "a00179" ],
    [ "TLS integration with ATECC (atcatls_)", "a00180.html", "a00180" ]
];