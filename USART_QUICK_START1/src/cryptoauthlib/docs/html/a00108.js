var a00108 =
[
    [ "max", "a00178.html#gaffe776513b24d84b39af8ab0930fef7f", null ],
    [ "min", "a00178.html#gac6afabdc09a49a433ee19d8a9486056d", null ],
    [ "hal_kit_cdc_idle", "a00178.html#ga92055a86e32f3dfc9eef064d48aa0870", null ],
    [ "hal_kit_cdc_init", "a00178.html#ga313bdfc8337f930a7f9fc6ca4e7731d9", null ],
    [ "hal_kit_cdc_post_init", "a00178.html#gad11b8714818c2b05f70593c002fc5945", null ],
    [ "hal_kit_cdc_receive", "a00178.html#ga3f3972f81de141ff0816b605054d775a", null ],
    [ "hal_kit_cdc_release", "a00178.html#ga21a939cc86a0602cb2ba3be3eec26e6e", null ],
    [ "hal_kit_cdc_send", "a00178.html#ga7732806c89cd206a96b7fabbaf6770d5", null ],
    [ "hal_kit_cdc_sleep", "a00178.html#ga1299c47e6d73d987bc04b16f25637bf6", null ],
    [ "hal_kit_cdc_wake", "a00178.html#gabced238b72e636a557d00355a2e44ac9", null ],
    [ "hal_kit_phy_num_found", "a00178.html#ga5fac3cf3552eee701aec448c1392edd8", null ],
    [ "kit_phy_receive", "a00178.html#gabb507252b1011037d6d2cce7d91b01d0", null ],
    [ "kit_phy_send", "a00178.html#gabd452e3edb32ea0d22653c182b4e1198", null ],
    [ "_gCdc", "a00178.html#gabc65c7c6bd64530b95fa72082f1e0390", null ],
    [ "dev", "a00178.html#gabe78755474c1323a5ac7b3dd6d03dedf", null ],
    [ "speed", "a00178.html#ga218b4f7c6cc2681a99c23a3b089d68b1", null ]
];