var searchData=
[
  ['i2c_5fbus_5fref_5fct',['i2c_bus_ref_ct',['../a00178.html#gaa7deec7f5d89dfb4f9746d050b0926f9',1,'i2c_bus_ref_ct():&#160;hal_sam4s_i2c_asf.c'],['../a00178.html#gaa7deec7f5d89dfb4f9746d050b0926f9',1,'i2c_bus_ref_ct():&#160;hal_samd21_i2c_asf.c'],['../a00178.html#gaa7deec7f5d89dfb4f9746d050b0926f9',1,'i2c_bus_ref_ct():&#160;hal_samd21_i2c_start.c'],['../a00178.html#gaa7deec7f5d89dfb4f9746d050b0926f9',1,'i2c_bus_ref_ct():&#160;hal_xmega_a3bu_i2c_asf.c']]],
  ['i2c_5fhal_5fdata',['i2c_hal_data',['../a00178.html#ga95dac4460cd54b4b073285ebc79d215b',1,'i2c_hal_data():&#160;hal_sam4s_i2c_asf.c'],['../a00178.html#ga95dac4460cd54b4b073285ebc79d215b',1,'i2c_hal_data():&#160;hal_samd21_i2c_asf.c'],['../a00178.html#ga95dac4460cd54b4b073285ebc79d215b',1,'i2c_hal_data():&#160;hal_samd21_i2c_start.c'],['../a00178.html#ga95dac4460cd54b4b073285ebc79d215b',1,'i2c_hal_data():&#160;hal_xmega_a3bu_i2c_asf.c']]],
  ['i2c_5fmaster_5finstance',['i2c_master_instance',['../a00044.html#a6fa5d175fefe82a0c13cc0f7afbf5593',1,'atcaI2Cmaster::i2c_master_instance()'],['../a00044.html#a335b4a621ab538c5a42160a5a14c161f',1,'atcaI2Cmaster::i2c_master_instance()'],['../a00044.html#ad60966bca127551f6271719dd9921045',1,'atcaI2Cmaster::i2c_master_instance()']]],
  ['id',['id',['../a00036.html#a0b1ca4dcd178907e4151c7132e3b55f5',1,'atcacert_cert_element_s']]],
  ['idx',['idx',['../a00045.html#ae40354a1051342eb5a9db005715dcfa9',1,'ATCAIfaceCfg']]],
  ['iface_5ftype',['iface_type',['../a00045.html#a3d0753b214d2a12df80f22b56bfc6e71',1,'ATCAIfaceCfg']]],
  ['input_5fdata',['input_data',['../a00031.html#a9791be4838266c9044e893a6c63e309d',1,'atca_write_mac_in_out']]],
  ['int_5fdata',['int_data',['../a00039.html#a770db19b08ef5a4f52bc606f8856406c',1,'atcacert_der_dec_integer__good_s']]],
  ['int_5fdata_5fsize',['int_data_size',['../a00039.html#a818c8bb9dd41b597a2f2641dbcb3c569',1,'atcacert_der_dec_integer__good_s']]],
  ['is_5fdevice_5fsn',['is_device_sn',['../a00035.html#a3969ddf030fd0524b62c572070bb3edc',1,'atcacert_build_state_s']]],
  ['is_5fgenkey',['is_genkey',['../a00041.html#ab0cedc80cd8670d02eee4b6e31500f5f',1,'atcacert_device_loc_s']]],
  ['issue_5fdate_5fformat',['issue_date_format',['../a00038.html#a61f951f9c4366391012057d591888f32',1,'atcacert_def_s']]]
];
