var indexSectionsWithContent =
{
  0: "_abcdefghiklmnoprstuvwz",
  1: "_acghs",
  2: "achklrstu",
  3: "_acdghiknprstu",
  4: "_abcdeghiklmnoprstuvwz",
  5: "_acghpu",
  6: "_aeu",
  7: "acdgmsuw",
  8: "_abcdefghilmnprstuvw",
  9: "abchst",
  10: "abch"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines",
  9: "groups",
  10: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros",
  9: "Modules",
  10: "Pages"
};

