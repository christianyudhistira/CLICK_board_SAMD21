var a00122 =
[
    [ "hal_kit_cdc_idle", "a00122.html#a92055a86e32f3dfc9eef064d48aa0870", null ],
    [ "hal_kit_cdc_init", "a00122.html#a313bdfc8337f930a7f9fc6ca4e7731d9", null ],
    [ "hal_kit_cdc_post_init", "a00122.html#ad11b8714818c2b05f70593c002fc5945", null ],
    [ "hal_kit_cdc_receive", "a00122.html#a3f3972f81de141ff0816b605054d775a", null ],
    [ "hal_kit_cdc_release", "a00122.html#a21a939cc86a0602cb2ba3be3eec26e6e", null ],
    [ "hal_kit_cdc_send", "a00122.html#a7732806c89cd206a96b7fabbaf6770d5", null ],
    [ "hal_kit_cdc_sleep", "a00122.html#a1299c47e6d73d987bc04b16f25637bf6", null ],
    [ "hal_kit_cdc_wake", "a00122.html#abced238b72e636a557d00355a2e44ac9", null ],
    [ "hal_kit_phy_num_found", "a00122.html#a5fac3cf3552eee701aec448c1392edd8", null ],
    [ "kit_phy_receive", "a00122.html#abb507252b1011037d6d2cce7d91b01d0", null ],
    [ "kit_phy_send", "a00122.html#abd452e3edb32ea0d22653c182b4e1198", null ],
    [ "_gCdc", "a00122.html#abc65c7c6bd64530b95fa72082f1e0390", null ]
];