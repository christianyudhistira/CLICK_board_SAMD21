var NAVTREE =
[
  [ "CryptoAuthLib", "index.html", [
    [ "CryptoAuthLib - Atmel CryptoAuthentication Library", "index.html", null ],
    [ "app directory - Purpose", "a00002.html", null ],
    [ "License", "a00004.html", null ],
    [ "basic directory - Purpose", "a00006.html", null ],
    [ "crypto directory - Purpose", "a00008.html", null ],
    [ "hal directory - Purpose", "a00010.html", null ],
    [ "Modules", "modules.html", "modules" ],
    [ "Data Structures", "annotated.html", [
      [ "Data Structures", "annotated.html", "annotated_dup" ],
      [ "Data Structure Index", "classes.html", null ],
      [ "Data Fields", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Variables", "functions_vars.html", "functions_vars" ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ],
      [ "Globals", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions", "globals_func.html", "globals_func" ],
        [ "Variables", "globals_vars.html", null ],
        [ "Typedefs", "globals_type.html", null ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", null ],
        [ "Macros", "globals_defs.html", "globals_defs" ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"a00002.html",
"a00057.html#abd3cc3148bdcaf38480e8762ceb749f2",
"a00062.html#a83a00fa1769c54aeb5ad02bdfba5b3e7",
"a00087.html#a6bff6a3fb624ab55c56a040df9dd13f8",
"a00148.html#a0a01eebe6fbf58725c5adae7b8fac31e",
"a00150.html#ab018d5b7f9578231e8c36a2defc34b66",
"a00165.html#a05e48cd5ef31c6735c83cd7d49ab612d",
"a00170.html#a001b74eeb369acbff95b0a1136367853",
"a00172.html#gac6e32aa207e97106e0921a3dd174ae74",
"a00175.html#ga9883eaf43aa94e7a29c1bdf13514185c",
"a00176.html#ga7b14188470aca1152fd3dbccb0043549",
"a00178.html#ga6cdff3589b286ebcdd7771bb425fbf73",
"a00179.html#ga3159b409bf2b94989d7dc00ba24f613d",
"a00180.html#ga9d95772e57a729d564fb1aa461594c40"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';